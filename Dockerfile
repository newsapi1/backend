FROM node:14

# Set the working directory in the container
WORKDIR /usr/src/app

# Copy the current directory contents into the container
COPY . .

# Install any dependencies
RUN npm install

# Make port 3000 available to the outside from this container
EXPOSE 3000

# Define the command to run your app (don't use npm start here)
CMD ["node", "./bin/www"]
